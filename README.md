# ArgoCD 101

This repository shows an example of how to manage clusters and applications using ArgoCD.

## Central Cluster

First step is to create the cluster that will host all central services - ArgoCD, Thanos, Argo Workflows.
```
openstack coe cluster create --cluster-template kubernetes-1.25.3-3 --keypair rocha-cern --node-count 3 --flavor m2.large --master-flavor m2.large --merge-labels webinar-central3
```

kubect label node ..-0 role=ingress
openstack server set --property landb-alias=webinar-central3-thanos-sidecar,webinar-central3-prometheus webinar-central3-jdztdvws2gf3-node-0

## Bootstrap

There are two bootstrap applications required - Vault and ArgoCD - that will be later also onboarded in the automated setup.

### Vault

#### Deploy

```
kubectl create namespace vault
helm repo add hashicorp https://helm.releases.hashicorp.com
helm -n vault install vault hashicorp/vault --values srv/vault/values.yaml
```

#### Unseal

```bash
kubectl -n vault exec -it vault-0 -- vault operator init

Unseal Key 1: ZjDlOfzThT71UjrYA9ejErGLeonZdKk9vdkNv4fHjflw
Unseal Key 2: 8zobbnPtHO1bVnmJXR8zRG2KKligspAJ2K1xaCPgqu60
Unseal Key 3: KI+o1Me0+yrLu6fK1gnKL832CWllOXQt1RIYG+d+wTxD
Unseal Key 4: /dXaFmxZ0KQ1b1VOkklpASlLhVEEHBOtkLp8u2w0s5tO
Unseal Key 5: 9CsMANN+ciPklMQTEYdDN/KbgijLV6D6mHNKUkaYgdnF

Initial Root Token: hvs.9UK61YKVNppyA127pfkQWsUU

Vault initialized with 5 key shares and a key threshold of 3. Please securely
distribute the key shares printed above. When the Vault is re-sealed,
restarted, or stopped, you must supply at least 3 of these keys to unseal it
before it can start servicing requests.

Vault does not store the generated root key. Without at least 3 keys to
reconstruct the root key, Vault will remain permanently sealed!

It is possible to generate new unseal keys, provided you have a quorum of
existing unseal keys shares. See "vault operator rekey" for more information.
```

```bash
kubectl -n vault exec -ti vault-0 -- vault operator unseal <key-1>
kubectl -n vault exec -ti vault-0 -- vault operator unseal <key-2>
kubectl -n vault exec -ti vault-0 -- vault operator unseal <key-3>
```

#### Root Token

```bash
kubectl -n vault exec -it vault-0 -- sh
/ $ vault login token=<initial root token>
/ $ vault token create
Key                  Value
---                  -----
token                hvs.302Sazfzp1U8FGp93KKYdSsC
```

#### KV Backend

```bash
/ $ vault secrets enable -version=2 kv
```

```bash
/ $ vault kv put kv/test-secret foo=bar
```

### ArgoCD

#### Vault Credentials

```bash
kubectl create namespace argocd
```
```
kubectl apply -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: argocd-vault-plugin-credentials
  namespace: argocd
type: Opaque
stringData:
  AVP_TYPE: vault
  AVP_AUTH_TYPE: token
  VAULT_ADDR: "http://$(kubectl -n vault get service -o jsonpath='{.items[?(@.metadata.name == "vault")].spec.clusterIP}'):8200"
  VAULT_TOKEN: "$(kubectl -n vault exec -it vault-0 -- vault token create -field=token)"
EOF
```

#### Install

```bash
helm repo add argocd https://argoproj.github.io/argo-helm
helm -n argocd install argocd srv/argocd
```

#### Login

```
kubectl -n argocd exec -it argocd-server-5745d56587-xfnll -- argocd admin initial-password
QvkTZy4sERGTVwZz
```

```
kubectl -n argocd port-forward svc/argocd-server 9999:443
```

The ArgoCD application should be available in http://localhost:9999 and login using the password above.

Alternatively (see comments in the ArgoCD values) you can setup an Ingress and SSO.

#### Onboard

```
kubectl apply -f main.yaml
```

### Workflows

To access the workflows dashboard, create a service account, role, and binding.
```
kubectl create sa workflows-webui
kubectl create clusterrolebinding workflows-webui --clusterrole=cluster-admin --serviceaccount=default:workflows-webui
kubectl apply -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: workflows-webui
  annotations:
    kubernetes.io/service-account.name: workflows-webui
type: kubernetes.io/service-account-token
EOF
ARGO_TOKEN="Bearer $(kubectl get secret workflows-webui -o=jsonpath='{.data.token}' | base64 --decode)"
echo $ARGO_TOKEN
```

## Applications

### Vault Secrets

```bash
kubectl -n vault exec -it vault-0 -- sh
/ $ vault kv put kv/services/myservice/mysecret foo=bar
/ $ vault kv patch kv/services/myservice/mysecret foo2=bar2
/ $ vault kv get kv/services/myservice/mysecret
```

To be reference in helm charts as in:
```
<path:kv/data/services/myservice/mysecret#foo>
```

